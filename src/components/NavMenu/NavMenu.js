import React from 'react';
import {Link, NavLink} from "react-router-dom";

import logo from "../../assets/326Power_logo.png";
import "./NavMenu.css";

const NavMenu = ({menu}) => {
  const menuItems = menu.map(item => {
    return (
        <li
            key={item["title"]}
        >
          <NavLink className="NavItem" exact to={item["path"]}>{item["title"]}</NavLink>
        </li>
    );
  });

  return (
      <div className="NavMenuBlock">
        <div className="container NavMenu">
          <Link to="/"><img className="NavLogo" src={logo} alt="326Power"/></Link>
          <ul className="Navigation">
            {menuItems}
          </ul>
        </div>
      </div>
  );
};

export default NavMenu;