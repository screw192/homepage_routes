import React from 'react';
import {Link} from "react-router-dom";

import "./Footer.css";
import fb from "../../assets/footer/facebook-brands.svg";
import ig from "../../assets/footer/instagram-brands.svg";
import yt from "../../assets/footer/youtube-brands.svg";

const Footer = () => {
  return (
      <div className="FooterBlock">
        <div className="container">
          <div className="Footer">
            <div className="FooterNav">
              <Link to="/about" className="FooterNavItem">About us</Link>
              <Link to="/contact" className="FooterNavItem">Contact Us</Link>
              <Link to="/" className="FooterNavItem">FAQ</Link>
              <Link to="/" className="FooterNavItem">Terms of service</Link>
              <Link to="/" className="FooterNavItem">Company policy & disclaimer</Link>
              <Link to="/" className="FooterNavItem">Returns and refund</Link>
            </div>
            <div className="FooterSocials">
              <a href="https://www.facebook.com/326powercojp/">
                <img src={fb} alt="Facebook" height="30px" width="auto"/>
              </a>
              <a href="https://www.instagram.com/326power_japan/">
                <img src={ig} alt="Instagram" height="30px" width="auto"/>
              </a>
              <a href="https://www.youtube.com/user/326POWEResl">
                <img src={yt} alt="YouTube" height="30px" width="auto"/>
              </a>
            </div>
            <div className="FooterCredits">
              <p>© 2021, 326Power USA. Designed by Sweet Bread 1337 LTD.</p>
            </div>
          </div>
        </div>
      </div>
  );
};

export default Footer;