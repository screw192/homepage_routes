import React from 'react';

import "./AboutUs.css";

const AboutUs = () => {
  return (
      <div className="AboutUsBlock">
        <div className="container_narrow">
          <div className="AboutUs">
            <h3 className="AboutUsTitle">About us</h3>
            <p className="AboutUsText">326Power USA is 326Power Japan’s direct distributing shop for USA customers. Our goal
              is to introduce and deliver 326Power Products to our customers fast and easily.</p>
            <p className="AboutUsText">We hope our customers in the USA can experience our high quality products. While we
              assist you with your vehicle upgrades to create your own style. All of our 326Power product can be purchased
              and ship directly to you from Japan.</p>
          </div>
          <div className="AboutUsContacts">
            <p className="AboutUsText">Email:
              <a className="AboutUsEmail" href="mailto:">info@326powerUSA.com</a>
            </p>
            <p className="AboutUsText">326Power USA</p>
            <p className="AboutUsText">6951 E. Dst Tacoma WA 98404</p>
          </div>
        </div>
      </div>
  );
};

export default AboutUs;