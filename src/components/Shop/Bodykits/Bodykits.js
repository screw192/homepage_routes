import React from 'react';
import ITEMS from "../shop_items";

const Bodykits = props => {
  const backToShop = () => {
    props.history.replace("/shop");
  };

  const kits = ITEMS.bodykits.map((item, index) => {
    return (
        <div key={"item" + index} className="ItemCard">
          <img className="ShopImageBlock_item" src={item.thumb} alt={item.name}/>
          <p className="ItemName">{item.name}</p>
          <p className="ItemPrice">
            <span className="PriceFrom">from </span>{item.startPrice}
          </p>
        </div>
    );
  });

  return (
      <div className="CategoryBlock">
        <div className="container">
          <div className="Category">
            {kits}
          </div>
          <button className="BackButton" onClick={backToShop}>Go back</button>
        </div>
      </div>
  );
};

export default Bodykits;