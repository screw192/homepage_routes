import kit_ls326s14zenki from "../../assets/shop/bodykits/ls326s14zenki_thumb.jpg";
import kit_ls326s15 from "../../assets/shop/bodykits/ls326s15_thumb.jpg";
import kit_3dstar180sx from "../../assets/shop/bodykits/3dstar180sx_thumb.jpg";
import kit_3dstarjzs16 from "../../assets/shop/bodykits/3dstarjzs16_thumb.png";
import kit_3dstars15 from "../../assets/shop/bodykits/3dstars15_thumb.jpg";
import kit_3dstars14zenki from "../../assets/shop/bodykits/3dstars14zenki_thumb.png";
import kit_gachabarigt86 from "../../assets/shop/bodykits/gachabarigt86_thumb.jpg";

import wing_manrikis15 from "../../assets/shop/spoilers/manrikis15logo.jpg";
import wing_gachabarigt86 from "../../assets/shop/spoilers/gachabarigt86.jpg";
import wing_manrikis15plus from "../../assets/shop/spoilers/manrikis15plus.jpg";
import wing_3dstargtr35 from "../../assets/shop/spoilers/3dstargtr35.jpg";

const ITEMS = {
  bodykits: [
    {
      name: "326POWER LS3.26 Zenki Nissan S14 Body Kit",
      thumb: kit_ls326s14zenki,
      startPrice: "$480"
    },
    {
      name: "326POWER LS3.26 Nissan S15 Body Kit",
      thumb: kit_ls326s15,
      startPrice: "$480"
    },
    {
      name: "326POWER Nissan 180SX 3D ☆ STAR Aero 3-piece Set",
      thumb: kit_3dstar180sx,
      startPrice: "$482"
    },
    {
      name: "326POWER 3D☆STAR Body Kit for Lexus GS300/Toyota Aristo JZS161",
      thumb: kit_3dstarjzs16,
      startPrice: "$480"
    },
    {
      name: "326POWER 3D☆STAR Body Kit for Nissan S15",
      thumb: kit_3dstars15,
      startPrice: "$480"
    },
    {
      name: "326POWER 3D☆STAR S14 Zenki Body Kit",
      thumb: kit_3dstars14zenki,
      startPrice: "$480"
    },
    {
      name: "326POWER【がちゃバリWIDE】Gachabari Toyota GT86 Widebody Conversion",
      thumb: kit_gachabarigt86,
      startPrice: "$1440"
    },
  ],
  wings_spoilers: [
    {
      name: "326POWER Manriki Wing with Logo (Nissan S15)",
      thumb: wing_manrikis15,
      price: "$566"
    },
    {
      name: "326POWER Gachabari Toyota 86/FRS/BRZ Rear Wing",
      thumb: wing_gachabarigt86,
      price: "$1199"
    },
    {
      name: "326POWER Manriki Rear Wing + (PLUS)",
      thumb: wing_manrikis15plus,
      price: "$1068"
    },
    {
      name: "326POWER 3D☆STAR Trunk Spoiler for Nissan R35 GT-R",
      thumb: wing_3dstargtr35,
      price: "$824"
    }
  ],
  wheels: {},
  coilovers: {},
  brakes: {},
  wheel_nuts: {},
};

export default ITEMS;