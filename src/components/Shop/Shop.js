import React from 'react';

import "./Shop.css";
import bodykits from '../../assets/shop/cat_bodykits.jpg';
import wingsSpoilers from '../../assets/shop/cat_wings_spoilers.jpg';
import wheels from '../../assets/shop/cat_wheels.jpg';
import coilovers from '../../assets/shop/cat_coilovers.jpg';
import brakes from '../../assets/shop/cat_brakes.jpg';
import nuts from '../../assets/shop/cat_nuts.jpg';

const Shop = props => {
  const catBodykits = () => {
    props.history.replace("/shop/bodykits");
  };
  const catWingsSpoilers = () => {
    props.history.replace("/shop/wingsandspoilers");
  };
  const catWheels = () => {
    props.history.replace("/shop/wheels");
  };
  const catCoilovers = () => {
    props.history.replace("/shop/coilovers");
  };
  const catBrakes = () => {
    props.history.replace("/shop/brakes");
  };
  const catNuts = () => {
    props.history.replace("/shop/wheelnuts");
  };

  return (
      <div className="ShopBlock">
        <div className="container">
          <div className="Shop">
            <div className="ShopCard">
              <div onClick={catBodykits} className="ShopCategory">
                <img className="ShopImageBlock" src={bodykits} alt="Bodykits"/>
                <p className="CategoryName">Bodykits</p>
              </div>
            </div>
            <div className="ShopCard">
              <div onClick={catWingsSpoilers} className="ShopCategory">
                <img className="ShopImageBlock" src={wingsSpoilers} alt="Wings & Spoilers"/>
                <p className="CategoryName">Wings & Spoilers</p>
              </div>
            </div>
            <div className="ShopCard">
              <div onClick={catWheels} className="ShopCategory">
                <img className="ShopImageBlock" src={wheels} alt="Wheels"/>
                <p className="CategoryName">Wheels</p>
              </div>
            </div>
            <div className="ShopCard">
              <div onClick={catCoilovers} className="ShopCategory">
                <img className="ShopImageBlock" src={coilovers} alt="Coilovers"/>
                <p className="CategoryName">Coilovers</p>
              </div>
            </div>

            <div className="ShopCard">
              <div onClick={catBrakes} className="ShopCategory">
                <img className="ShopImageBlock" src={brakes} alt="Brakes"/>
                <p className="CategoryName">Brakes</p>
              </div>
            </div>
            <div className="ShopCard">
              <div onClick={catNuts} className="ShopCategory">
                <img className="ShopImageBlock" src={nuts} alt="Wheel Nuts"/>
                <p className="CategoryName">Wheel Nuts</p>
              </div>
            </div>
          </div>
        </div>
      </div>
  );
};

export default Shop;