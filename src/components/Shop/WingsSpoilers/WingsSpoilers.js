import React from 'react';
import ITEMS from "../shop_items";

const WingsSpoilers = props => {
  const backToShop = () => {
    props.history.replace("/shop");
  };

  const wings = ITEMS.wings_spoilers.map((item, index) => {
    return (
        <div key={"item" + index} className="ItemCard">
          <img className="ShopImageBlock_item" src={item.thumb} alt={item.name}/>
          <p className="ItemName">{item.name}</p>
          <p className="ItemPrice">
            <span className="PriceFrom">from </span>{item.price}
          </p>
        </div>
    );
  });

  return (
      <div className="CategoryBlock">
        <div className="container">
          <div className="Category">
            {wings}
          </div>
          <button className="BackButton" onClick={backToShop}>Go back</button>
        </div>
      </div>
  );
};

export default WingsSpoilers;