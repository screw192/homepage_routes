import React from 'react';

import "./Home.css";
import mainBackground from "../../assets/main_background.jpg";
import gal_fit_00 from "../../assets/gallery/gallery_hondafit_00.jpg";
import gal_fit_01 from "../../assets/gallery/gallery_hondafit_01.jpg";
import gal_fit_02 from "../../assets/gallery/gallery_hondafit_02.jpg";
import gal_mzd6_00 from "../../assets/gallery/gallery_mazda6_00.jpg";
import gal_mzd6_01 from "../../assets/gallery/gallery_mazda6_01.jpg";
import gal_mzd6_02 from "../../assets/gallery/gallery_mazda6_02.jpg";
import gal_370_00 from "../../assets/gallery/gallery_nissan370_00.jpg";
import gal_370_01 from "../../assets/gallery/gallery_nissan370_01.jpg";
import gal_370_02 from "../../assets/gallery/gallery_nissan370_02.jpg";
import gal_s15_00 from "../../assets/gallery/gallery_nissans15_00.jpg";
import gal_s15_01 from "../../assets/gallery/gallery_nissans15_01.jpg";
import gal_s15_02 from "../../assets/gallery/gallery_nissans15_02.jpg";

const Home = () => {
  return (
      <div className="Home">
        <div className="MainBlock" style={{backgroundImage: `url(${mainBackground})`}}>
          <p className="MainBlockText">New Arrival</p>
        </div>
        <div className="container">
          <div className="DemoCarsBlock">
            <h2>Demo cars</h2>
            <div className="DemoCarsGallery">
              <div className="DemoCarBlock">
                <h3 className="DemoCarModel">Honda Fit</h3>
                <p className="DemoCarMods">326POWER Yabaking Spoke Wheels, 17×9.5J -13, 17×10.5J-26, Energy Mint Blue with Cut Anodised lips.</p>
                <div className="ImageBlock">
                  <img src={gal_fit_00} alt="Honda Fit"/>
                  <img src={gal_fit_01} alt="Honda Fit"/>
                  <img src={gal_fit_02} alt="Honda Fit"/>
                </div>
              </div>
              <div className="DemoCarBlock">
                <h3 className="DemoCarModel">Mazda 6</h3>
                <p className="DemoCarMods">326Power Yabaking VVIP Wheels 20 x 8.5 +40</p>
                <div className="ImageBlock">
                  <img src={gal_mzd6_00} alt="Mazda 6"/>
                  <img src={gal_mzd6_01} alt="Mazda 6"/>
                  <img src={gal_mzd6_02} alt="Mazda 6"/>
                </div>
              </div>
              <div className="DemoCarBlock">
                <h3 className="DemoCarModel">Nissan 370Z</h3>
                <p className="DemoCarMods">326POWER Chakuriki Coilovers</p>
                <p className="DemoCarMods">326POWER Yabaking Spoke Wheels, 19×12.0J-27, 19×12.0J-65, Black Clear Red with Brushed Matte Anodised lips</p>
                <p className="DemoCarMods">326POWER Wheel Nuts & Spike End Caps</p>
                <p className="DemoCarMods">326POWER Manriki Rear Wing</p>
                <div className="ImageBlock">
                  <img src={gal_370_00} alt="Nissan 370Z"/>
                  <img src={gal_370_01} alt="Nissan 370Z"/>
                  <img src={gal_370_02} alt="Nissan 370Z"/>
                </div>
              </div>
              <div className="DemoCarBlock">
                <h3 className="DemoCarModel">Nissan Silvia (s15)</h3>
                <p className="DemoCarMods">326POWER Chakuriki Coilovers</p>
                <p className="DemoCarMods">326POWER 3D☆STAR Body Kit for Nissan S15</p>
                <p className="DemoCarMods">326POWER Universal Lip Kit Type 2</p>
                <p className="DemoCarMods">326POWER Nissan S15 Roof Spoiler</p>
                <p className="DemoCarMods">326POWER Manriki Wing (Universal)</p>
                <p className="DemoCarMods">326POWER D-rey Headlight Lens Covers</p>
                <p className="DemoCarMods">326POWER Yabaking 1-Piece Wheels Mitsuru Green with Polished Lip</p>
                <div className="ImageBlock">
                  <img src={gal_s15_00} alt="Nissan Silvia (s15)"/>
                  <img src={gal_s15_01} alt="Nissan Silvia (s15)"/>
                  <img src={gal_s15_02} alt="Nissan Silvia (s15)"/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  );
};

export default Home;