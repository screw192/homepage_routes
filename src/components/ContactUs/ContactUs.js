import React from 'react';

import "./ContactUs.css";

const ContactUs = () => {
  return (
      <div className="ContactUsBlock">
        <div className="container_narrow">
          <div className="ContactUs">
            <h3 className="ContactUsTitle">Contact us</h3>
            <p className="ContactUsText">Need help? Fill in the form below or email us directly at info@326powerusa.com.</p>
            <p className="ContactUsText">If you do not receive a reply, please check your Spam folder as our emails are
              sometimes directed there.</p>
            <input type="text" placeholder="Name"/>
            <input type="email" placeholder="E-mail"/>
            <textarea
                name="Message"
                id="ContactFormMessage"
                placeholder="Message"
                style={{resize: "none"}}/>
            <button className="SendButton">Send</button>
          </div>
        </div>
      </div>
  );
};

export default ContactUs;