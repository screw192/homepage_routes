import React from 'react';
import {Route, Switch} from "react-router-dom";

import "./App.css";

import NavMenu from "./components/NavMenu/NavMenu";
import Home from "./components/Home/Home";
import Shop from "./components/Shop/Shop";
import Bodykits from "./components/Shop/Bodykits/Bodykits";
import WingsSpoilers from "./components/Shop/WingsSpoilers/WingsSpoilers";
import ContactUs from "./components/ContactUs/ContactUs";
import AboutUs from "./components/AboutUs/AboutUs";
import Footer from "./components/Footer/Footer";

const App = () => {
  return (
      <div className="App">
        <NavMenu menu={[
          {path: "/", title: "Home"},
          {path: "/shop", title: "Shop"},
          {path: "/about", title: "About us"},
          {path: "/contact", title: "Contact Us"},
        ]}/>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/shop" exact component={Shop} />
            <Route path="/shop/bodykits" component={Bodykits} />
            <Route path="/shop/wingsandspoilers" component={WingsSpoilers} />
          <Route path="/about" component={AboutUs} />
          <Route path="/contact" component={ContactUs} />
        </Switch>
        <Footer />
      </div>
  );
}

export default App;